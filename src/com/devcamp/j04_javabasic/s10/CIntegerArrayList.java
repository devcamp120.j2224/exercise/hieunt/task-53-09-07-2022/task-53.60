package com.devcamp.j04_javabasic.s10;
import java.util.ArrayList;

public class CIntegerArrayList implements ISumable {
	private ArrayList<Integer> mIntegerArrayList;

	/* hàm khởi tạo có @param mIntegerArrayList */
	public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}

	@Override
	public String getSum() {
		int sum = 0;
		for (Integer i : this.mIntegerArrayList) {
			sum += i;
		}
		return "Đây là Sum của class CIntegerArrayList: " + sum;
	}

	/* hàm get mIntegerArrayList của class CIntegerArrayList */
	public ArrayList<Integer> getmIntegerArrayList() {
		return mIntegerArrayList;
	}

	/* hàm set mIntegerArrayList của class CIntegerArrayList */
	public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}
 
}
