package com.devcamp.j04_javabasic.s10;
public class CDog extends CPet implements IBarkable, IRunable, Other{
	@Override
	public void run() {
		System.out.println("Chos chay");
	}
	@Override
	public void running() {	}
	@Override
	public void bark() {
		System.out.println("Chos suar");
	}
	@Override
	public void barkBanrk() { }
	@Override
	public void animalSound() {
		this.bark();
	}
	@Override
	public void other() {
		System.out.println("Dog other");
	}
	@Override
	public int other(int param) {
		return 10;
	}
	@Override
	public String other(String param) {
		return "Something";
	}
	// hàm khởi tạo có 2 tham số
	public CDog(String name, int age) {
		this.name = name;
		this.age = age;
	}
}
