package com.devcamp.j04_javabasic.s10;
public class CFish extends CPet implements ISwimable {
	@Override
	public void swim() {
		System.out.println("fish swiming.");
	}
	public void animalSound() {
		System.out.println("Fish sound...");
	}
}
